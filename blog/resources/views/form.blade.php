<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Form</title>
</head>
<body>
   <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('welcome') }}" method="post">
        @csrf
        <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname"><br><br>
        <label for="gender">Gender:</label><br><br>
            <input type="radio" name="gender" id="male">
                <label for="male">Male</label><br>
            <input type="radio" name="gender" id="female">
                <label for="female">Female</label><br>
            <input type="radio" name="gender" id="other">
                <label for="other">Other</label>
                <br><br>
        <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
            </select>
            <br><br>
        <label for="spoken">Language Spoken:</label><br><br>
            <input type="checkbox" name="bahasa" id="">
                <label for="bahasa">Bahasa Indonesia</label><br>
            <input type="checkbox" name="english" id="">
                <label for="englsih">English</label><br>
            <input type="checkbox" name="other" id="">
                <label for="other">Other</label><br><br>
        <label for="bio">Bio:</label><br><br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea>
            <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>