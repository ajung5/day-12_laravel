<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/home', function () {
//     return view('home');
// });


// Route::get('/register', function () {
//     return view('form');
// });

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'AuthController@register')->name('register');
Route::get('/welcome', 'AuthController@welcome')->name('welcome');


Route::post('/welcome', 'AuthController@store')->name('welcome');